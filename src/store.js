import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tes: 'halo',
    storeSearch: '',
    nextWeathers: undefined,
    today: {},
    city: '',
    savedLocation: []
  },
  mutations: {
    addLocation (state, options) {
      let array = state.savedLocation
      array.push(options)
      state.savedLocation = array
      console.log(state.savedLocation)
    },
    setLocation (state, value) {
      state.savedLocation = value
    },
    setStoreSearch (state, value) {
      state.storeSearch = value
    },
    setNextWeathers (state, value) {
      state.nextWeathers = value
    },
    setToday (state, value) {
      state.today = value
    },
    setCity (state, value) {
      state.city = value
    }
  }
})
