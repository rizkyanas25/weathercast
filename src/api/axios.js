const axios = require('axios')

const ax = axios.create({
  baseURL: 'https://cors-anywhere.herokuapp.com/www.metaweather.com/'
})

module.exports = ax
