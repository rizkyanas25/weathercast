import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import SavedCity from './views/SavedCity.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/saved',
      name: 'saved',
      component: SavedCity
    }
  ]
})
